# Algorithms I Assignment 1: Percolation
Solution for assignment 1 of the Princeton University Algorithms I course on Coursera (https://coursera.cs.princeton.edu/algs4/assignments/percolation/specification.php).

## Results  

Correctness:  34/38 tests passed  

Memory:       8/8 tests passed  

Timing:       20/20 tests passed  

<b> Aggregate score:</b> 93.68%

## How to use
To run a Monte Carlo simulation testing for the average percolation threshold, ```PercolationStats.java``` can be run with two integer command line arguments > 0 (n and t) for the size of grid to be tested on and the number of trials.
Requires the algs4.jar library used in the course.

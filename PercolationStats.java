import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

public class PercolationStats {

    private final double[] fractionOpenCellsAtEnd;

    private int numOpenCells = 0;
    private final int size;
    private final int trials;
    private final static double CONFIDENCE_95 = 1.96;

    // perform independent trials on an n-by-n grid
    public PercolationStats(int n, int trials) {
        if (n <= 0 || trials <= 0) throw new IllegalArgumentException();

        this.size = n;
        this.trials = trials;
        fractionOpenCellsAtEnd = new double[trials];

        for (int t = 0; t < trials; t++) {
            Percolation p = new Percolation(size);
            while (!p.percolates()) {
                int row = StdRandom.uniform(1, size + 1);
                int col = StdRandom.uniform(1, size + 1);
                if (!p.isOpen(row, col)) {
                    this.numOpenCells++;
                    p.open(row, col);
                }
            }

            fractionOpenCellsAtEnd[t] = (double)
                    this.numOpenCells / (size * size);
            this.numOpenCells = 0;
        }
    }

    // sample mean of percolation threshold
    public double mean() {
        return StdStats.mean(this.fractionOpenCellsAtEnd);
    }

    // sample standard deviation of percolation threshold
    public double stddev() {
        if (trials == 1) {
            return Double.NaN;
        }
        return StdStats.stddev(this.fractionOpenCellsAtEnd);
    }

    // low endpoint of 95% confidence interval
    public double confidenceLo() {
        return this.mean() - ((CONFIDENCE_95 * this.stddev()) / Math.sqrt(trials));
    }

    // high endpoint of 95% confidence interval
    public double confidenceHi() {
        return this.mean() + ((CONFIDENCE_95 * this.stddev()) / Math.sqrt(trials));
    }

    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        int trials = Integer.parseInt(args[1]);

        PercolationStats percolationStats = new PercolationStats(n, trials);

        System.out.println(String.format("mean = %f",
                                         percolationStats.mean()));
        System.out.println(String.format("stddev = %f",
                                         percolationStats.stddev()));
        System.out.println(String.format("95%% confidence interval =  [%f, %f]",
                                         percolationStats.confidenceLo(),
                                         percolationStats.confidenceHi()));

    }
}

import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {

    private final int virtualBot;

    private final int virtualTop;

    private boolean[] openStatus;

    private final int size;

    private int numOpenSites;

    private final WeightedQuickUnionUF quickUnionUF;

    public Percolation(int n) {
        if (n <= 0) throw new IllegalArgumentException();
        size = n;
        numOpenSites = 0;
        openStatus = new boolean[n * n];
        quickUnionUF = new WeightedQuickUnionUF((n * n) + 2);
        virtualBot = 0;
        virtualTop = (n * n) + 1;
        for (int i = 1; i < n + 1; i++) {
            quickUnionUF.union(i, virtualTop);
        }
        if (n > 1) {
            for (int i = n * n; i > (n * n) - n; i--) {
                quickUnionUF.union(i, virtualBot);
            }
        }

    }

    // helper method to go from row/col pairs to the 1d array of the quickunion data structure
    private int mapTo1d(int row, int col) {
        return (row - 1) * size + col;
    }

    // helper method to check if coord pair is within range of grid
    private boolean validCoords(int row, int col) {
        return (row < 1 || row > size || col < 1 || col > size) ? false : true;
    }

    // opens the site (row, col) if it is not open already
    public void open(int row, int col) {
        if (row < 1 || row > size || col < 1 || col > size) throw new IllegalArgumentException();
        if (isOpen(row, col)) return;

        openStatus[mapTo1d(row, col) - 1] = true;
        numOpenSites++;

        if (size == 1) {
            quickUnionUF.union(mapTo1d(row, col), virtualBot);
            return;
        }

        int[][] neighbours = new int[][] {
                { row + 1, col }, { row - 1, col }, { row, col - 1 },
                { row, col + 1 }
        };
        for (int[] neighbour : neighbours
        ) {
            int nRow = neighbour[0];
            int nCol = neighbour[1];
            if (validCoords(nRow, nCol) && isOpen(nRow, nCol)) {
                quickUnionUF.union(mapTo1d(row, col), mapTo1d(nRow, nCol));
            }
        }
    }

    // is the site (row, col) open?
    public boolean isOpen(int row, int col) {
        if (row < 1 || row > size || col < 1 || col > size) throw new IllegalArgumentException();
        return openStatus[mapTo1d(row, col) - 1];
    }

    // is the site (row, col) full?
    public boolean isFull(int row, int col) {
        if (row < 1 || row > size || col < 1 || col > size) throw new IllegalArgumentException();
        if (isOpen(row, col)) {
            return quickUnionUF.find(mapTo1d(row, col)) == quickUnionUF.find(virtualTop);
        }
        return false;
    }

    // returns the number of open sites
    public int numberOfOpenSites() {
        return numOpenSites;
    }

    // does the system percolate?
    public boolean percolates() {
        return quickUnionUF.find(virtualTop) == quickUnionUF.find(virtualBot);
    }
}